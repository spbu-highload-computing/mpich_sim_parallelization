//
// Created by Max Nigmatulin on 01.12.2022.
//

#include "utils.h"
#include <random>
#include <iostream>


float **generate_matrix(int size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(0, 1);

    float **a = new float *[size];
    for (int i = 0; i < size; i++) {
        a[i] = new float[size];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == j) {
                a[i][j] = dist(gen);
            } else {
                a[i][j] = 0;
            }
        }
    }

    return a;
}

float *generate_vector(int size) {
    float *v = new float[size];
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> dist(1, 2); // define the range

    for (int i = 0; i < size; i++) {
        v[i] = dist(gen);
    }

    return v;
}

float *copy_vector(const float *v, int n) {
    float *new_v = new float[n];
    for (int i = 0; i < n; i++) {
        new_v[i] = v[i];
    }
    return new_v;
}

void print_float_vector(float *v, int n) {
    if (v == nullptr) {
        return;
    }
    std::cout << " [";
    for (int i = 0; i < n; i++) {
        std::cout << v[i] << " ";
    }
    std::cout << "]\n";
}
