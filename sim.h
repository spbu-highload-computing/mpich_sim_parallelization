//
// Created by Max Nigmatulin on 01.12.2022.
//

#ifndef HIGHLOAD_COMPUTING_2_SIM_H
#define HIGHLOAD_COMPUTING_2_SIM_H

float *simple_iterations_method_parallel(float **, float *, int, int);

float *simple_iterations_method(float **, float *, int, int);

#endif //HIGHLOAD_COMPUTING_2_SIM_H
