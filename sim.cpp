//
// Created by Max Nigmatulin on 01.12.2022.
//

#include "sim.h"
#include "mpi.h"
#include "utils.h"

float *simple_iterations_method_parallel(float **a, float *b, int n, int iterations = 10) {
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size); //get number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    float **H = new float *[n];
    for (int i = 0; i < n; i++) {
        H[i] = new float[n];
    }
    float *g = new float[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                H[i][j] = 0;
            } else {
                H[i][j] = -1 * a[i][j] / a[i][i];
            }
        }
        g[i] = b[i] / a[i][i];
    }
    float *x = new float[n];

    for (int k = 0; k < iterations; k++) {
        float *new_x = copy_vector(g, n);
        for (int ij = 0; ij < n * n; ij++) {
            int i = ij / n;
            if (ij % size != rank) continue;
            int j = ij % n;
            new_x[i] += H[i][j] * x[j];
        }
        for (size_t i = 0; i < n; i++) {
            x[i] = 0;
        }
        MPI_Allreduce(new_x, x, n, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
        x = new_x;
    }

    if (rank == 0) {
        return x;
    }
    return nullptr;
}

float *simple_iterations_method(float **a, float *b, int n, int iterations = 10) {

    float **H = new float *[n];
    for (int i = 0; i < n; i++) {
        H[i] = new float[n];
    }
    float *g = new float[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                H[i][j] = 0;
            } else {
                H[i][j] = -1 * a[i][j] / a[i][i];
            }
        }
        g[i] = b[i] / a[i][i];
    }
    float *x = new float[n];

    for (int k = 0; k < iterations; k++) {
        float *new_x = copy_vector(g, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                new_x[i] += H[i][j] * x[j];
            }
        }
        x = new_x;
    }

    return x;
}
