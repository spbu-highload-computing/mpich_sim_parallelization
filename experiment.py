import subprocess
from datetime import datetime
from pathlib import Path
import os
from shutil import rmtree
from time import sleep


class Experiment:

    def __init__(self, mpiexec_path: str, ofile_path: str, threads: int, rank: int, iterations_num: int,
                 exp_filename: str) -> None:
        # mpiexec -np 1 ./a.out 1000 1000 text.txt
        self.call_array = [str(item) for item in
                           [mpiexec_path, "-np", threads, ofile_path, rank, iterations_num, exp_filename]]

    def launch(self):
        subprocess.call(self.call_array)

    def print(self):
        print(*self.call_array)


# task 1
def construct_rank_experiment(
        mpiexec_path: str, ofile_path: str, ranks: [int], threads: [int], iterations_num: int,
        exp_filename_base="ranks_experiment"
) -> [Experiment]:
    experiments = []

    for rank in ranks:
        for thread in threads:
            experiments.append(
                Experiment(
                    mpiexec_path=mpiexec_path,
                    ofile_path=ofile_path,
                    threads=thread,
                    rank=rank,
                    iterations_num=iterations_num,
                    exp_filename=f"{exp_filename_base}_rank={rank}.txt"
                )
            )
    return experiments


# task 2
def construct_iterations_experiment(
        mpiexec_path: str, ofile_path: str, iterations: [int], threads: [int], rank: int,
        exp_filename_base="iterations_experiment"
) -> [Experiment]:
    experiments = []
    for iterations_num in iterations:
        for thread in threads:
            experiments.append(
                Experiment(
                    mpiexec_path=mpiexec_path,
                    ofile_path=ofile_path,
                    threads=thread,
                    rank=rank,
                    iterations_num=iterations_num,
                    exp_filename=f"{exp_filename_base}_iters={iterations_num}.txt"
                )
            )
    return experiments


def construct_amdahls_experiment(
        mpiexec_path: str, ofile_path: str, threads: [int], iterations_num: int, rank: int,
        exp_filename_base="amdahls_experiment"
) -> [Experiment]:
    experiments = []
    for thread in threads:
        experiments.append(
            Experiment(
                mpiexec_path=mpiexec_path,
                ofile_path=ofile_path,
                threads=thread,
                rank=rank,
                iterations_num=iterations_num,
                exp_filename=f"{exp_filename_base}.txt")
        )

    return experiments


def create_dir(path):
    if os.path.exists(path):
        rmtree(path)
    Path(path).mkdir(parents=True, exist_ok=True)


HOME_SYSTEM = False


def main():
    exec_path = "./a.out"
    compiler = "mpic++"
    mpiexec = "mpiexec"
    if HOME_SYSTEM:
        available_core_num = 8
    else:
        available_core_num = 6
    build_exec = [compiler, "main.cpp", "utils.cpp", "sim.cpp"]

    def time():
        return datetime.now().strftime("%H:%M:%S")

    experiment_threads = [i for i in range(1, available_core_num + 1)]

    create_dir("./ranks")
    thread_experiment_ranks = [3000, 5000, 10000, 15000, 20000]
    ranks_experiments = construct_rank_experiment(
        mpiexec_path=mpiexec,
        ofile_path=exec_path,
        ranks=thread_experiment_ranks,
        threads=experiment_threads,
        iterations_num=10,
        exp_filename_base="./ranks/ranks_exp"
    )

    create_dir("./iter")
    thread_experiment_iterations = [1000, 2000, 3000, 4000]
    iterations_experiments = construct_iterations_experiment(
        mpiexec_path=mpiexec,
        ofile_path=exec_path,
        iterations=thread_experiment_iterations,
        threads=experiment_threads,
        rank=200,
        exp_filename_base="./iter/iterations_exp"
    )

    amdahls_law_check = construct_amdahls_experiment(
        mpiexec_path=mpiexec,
        ofile_path=exec_path,
        threads=experiment_threads,
        iterations_num=100,
        rank=10000,
        exp_filename_base="amdahls_law_check"
    )

    subprocess.call(build_exec)

    print(time() + ": ranks experiment")
    for experiment in ranks_experiments:
        experiment.launch()

    print(time() + ": iterations experiment")
    for experiment in iterations_experiments:
        experiment.launch()

    print(time() + ": amdahls law check")
    for experiment in amdahls_law_check:
        experiment.launch()


if __name__ == "__main__":
    main()
