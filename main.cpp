#include <iostream>
#include <fstream>
#include <cstring>
#include <chrono>
#include "mpi.h"
#include "sim.h"
#include "utils.h"


void compute_experiment(float **a, float *b, int thread_num, int matrix_rank, int iterations,
                        float *(*func)(float **, float *, int, int), std::basic_ostream<char>& ostream) {
    auto start_parallel = std::chrono::high_resolution_clock::now();
    float *result_parallel = func(a, b, matrix_rank, iterations);
    auto stop_parallel = std::chrono::high_resolution_clock::now();

    if (result_parallel == nullptr) {
        return;
    }
    auto time = std::chrono::duration_cast<std::chrono::microseconds>(stop_parallel - start_parallel).count();
    ostream << "(threads=" << thread_num << "): ";
    ostream << "rank=" << matrix_rank << " ";
    ostream << "iterations=" << iterations << " ";
    ostream << "time=" << (float) time / 1000000 << std::endl;
}

int main(int argc, char *argv[]) {
    // 0: program path
    // 1: matrix rank
    // 2: iteration count
    // 3: stats_filename

    int matrix_rank = atoi(argv[1]);
    int iterations = atoi(argv[2]);
    char *stats_filename = argv[3];

    auto a = generate_matrix(matrix_rank);
    auto b = generate_vector(matrix_rank);

    MPI_Init(&argc, &argv); //initialize MPI library
    int thread_id, thread_num;
    MPI_Comm_size(MPI_COMM_WORLD, &thread_num); //get number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &thread_id);

    if (thread_id == 0) {
        std::cout << "thread_num: " << thread_num << " ";
        std::cout << "matrix_rank: " << matrix_rank << " ";
        std::cout << "iterations: " << iterations << "\n";
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if (thread_num == 1) {
        std::ofstream stats_file(stats_filename, std::ios::app);
        compute_experiment(a, b, 1, matrix_rank, iterations, &simple_iterations_method, stats_file);
    }
    else {
        std::ofstream stats_file(stats_filename, std::ios::app);
        compute_experiment(a, b, thread_num, matrix_rank, iterations, &simple_iterations_method_parallel, stats_file);
    }

    MPI_Finalize();
    return 0;
}
