//
// Created by Max Nigmatulin on 01.12.2022.
//

#ifndef HIGHLOAD_COMPUTING_2_UTILS_H
#define HIGHLOAD_COMPUTING_2_UTILS_H


float **generate_matrix(int size);

float *generate_vector(int size);

float *copy_vector(const float *v, int n);

void print_float_vector(float *v, int n);

#endif //HIGHLOAD_COMPUTING_2_UTILS_H
